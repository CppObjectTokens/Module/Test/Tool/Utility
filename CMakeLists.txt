# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

cmake_minimum_required(VERSION 3.13...3.14.2)

project(Test.Tool.Utility CXX)

include(Molurus.Module)

add_molurus_interface_library(${PROJECT_NAME})
