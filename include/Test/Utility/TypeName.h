/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <string>
#include <string_view>

namespace Test
{

namespace Utility
{

namespace Detail
{

template <class T>
constexpr std::string_view typeName()
{
    std::string_view name, prefix, suffix;

#ifdef __clang__
    name   = __PRETTY_FUNCTION__;
    prefix = "std::string_view Test::Utility::Detail::typeName() [T = ";
    suffix = "]";
#elif defined (__GNUC__)
    name   = __PRETTY_FUNCTION__;
    prefix = "constexpr std::string_view Test::Utility::Detail::typeName()"
        " [with T = ";
    suffix = "; std::string_view = std::basic_string_view<char>]";
#elif defined (_MSC_VER)
    name   = __FUNCSIG__;
    prefix = "class std::basic_string_view<char,struct std::char_traits<char> >"
        " __cdecl Test::Utility::Detail::typeName<";
    suffix = ">(void)";
#endif

    name.remove_prefix(prefix.size());
    name.remove_suffix(suffix.size());

    return name;
}

} // namespace Detail

template <class T>
inline
std::string typeName()
{ return std::string{Detail::typeName<T>()}; }

} // namespace Utility

} // namespace Test
